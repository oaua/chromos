package main

import (
	"chromos/lib"
	"flag"
	"fmt"
	"io"
	"net/http"
	"os"
	"strings"
	"time"
)

func main() {

	// initialize flags
	var outFlag string
	flag.StringVar(&outFlag, "o", "stdout", "File to ouput, default to stdout")

	var startServer bool
	flag.BoolVar(&startServer, "s", false, "Open a server http")

	flag.Parse()

	selectedColor := lib.White
	selectedColorString := "White (default)"

	// get color in the first argument or use white as default
	args := flag.Args()
	if len(args) > 0 {
		colorArg := strings.Join(args, " ")
		selectedColor, selectedColorString = lib.StringToColor(colorArg)
	}

	lib.PrintLn(selectedColor, selectedColorString)

	serverError := make(chan error)
	// http server
	if startServer {
		lib.PrintLn(selectedColor, "server listen on 8000")
		http.HandleFunc("/", getColoredTimeHandler(selectedColor))
		go runServer(serverError)
		// log.Fatal(http.ListenAndServe(":8000", nil))
	}

	// get writer
	writer := getOutWriter(outFlag)

	go runTimeLoop(selectedColor, writer)
	// for {
	// 	printColoredTime(selectedColor, writer)
	// 	time.Sleep(time.Second)
	// }

	lib.PrintLn(selectedColor, "Server fail: ", <-serverError)

}

func getOutWriter(out string) io.Writer {
	if out != "stdout" {
		file, err := os.OpenFile(out, os.O_WRONLY|os.O_APPEND|os.O_CREATE, 0664)
		if err != nil {
			fmt.Printf("out to: %s (%s)", "stdout", err)
			return os.Stdout
		}
		fmt.Printf("out to file: %s", out)
		return file
	} else {
		fmt.Printf("out to: %s\n", "stdout")
		return os.Stdout
	}
}

func printColoredTime(color lib.ShellColor, writer io.Writer) {

	// get format option
	showDate := os.Getenv("SHOW_DATE")
	var format string

	// set format
	if showDate == "true" {
		format = "02/01/2006 - 15:04:05"
	} else {
		format = "15:04:05"
	}

	// Mon Jan 2 15:04:05 MST 2006
	timeString := time.Now().Format(format)

	lib.FprintLn(color, writer, timeString)
}

func getColoredTimeHandler(color lib.ShellColor) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, _ *http.Request) {
		printColoredTime(color, w)
	}
}

func runTimeLoop(selectedColor lib.ShellColor, writer io.Writer) {
	for {
		printColoredTime(selectedColor, writer)
		time.Sleep(time.Second)
	}
}

func runServer(serverError chan error) {
	serverError <- http.ListenAndServe(":8000", nil)
}
