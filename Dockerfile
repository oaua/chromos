FROM golang:bookworm AS base

### Go tools Specific install
RUN \
    # go language server: used by vscode to work with go
    go install golang.org/x/tools/gopls@latest \
    # go auto import and formating
    && go install golang.org/x/tools/cmd/goimports@latest \
    # # go linter
    && go install honnef.co/go/tools/cmd/staticcheck@latest \
    # # go debugger
    && go install github.com/go-delve/delve/cmd/dlv@latest \
    # # go auto reload
    && go install github.com/cosmtrek/air@latest

# Base directory config
ARG APP_PATH=/app
WORKDIR $APP_PATH

#################################################################"
FROM base AS build 

WORKDIR /app

COPY . .
# build go
RUN go build -o chromos .


#################################################################"
FROM debian:bookworm-slim AS prod 

# CONFIGS
ARG USER_NAME=runner
ARG USER_ID=1000
ARG GROUP_NAME=$USER_NAME
ARG GROUP_ID=$USER_ID

ARG APP_PATH=/app
WORKDIR $APP_PATH

RUN apt-get update && apt-get upgrade -y && apt-get install -y \
    dnsutils \
    curl

# debian
RUN addgroup --gid ${GROUP_ID} ${GROUP_NAME} \
    && adduser --uid ${USER_ID} --gid ${GROUP_ID} ${USER_NAME} \
    && chown ${USER_ID}:${GROUP_ID} ${APP_PATH}
# alpine
# RUN addgroup -g ${GROUP_ID} ${GROUP_NAME} \
#     && adduser -u ${USER_ID} -G ${GROUP_NAME} -D -s /bin/sh ${USER_NAME}

COPY --from=build --chown=${USER_ID}:${GROUP_ID} --chmod=500 /app/chromos /app/chromos

ENV SHOW_DATE="true"

ENTRYPOINT ["/app/chromos"]
EXPOSE 8000
USER $USER_NAME

#################################################################"
FROM base AS dev

# CONFIGS
ARG USER_NAME=dev
ARG USER_ID=1000
ARG GROUP_NAME=$USER_NAME
ARG GROUP_ID=$USER_ID

# Update & dev tool install
RUN apt-get update && apt-get upgrade -y && apt-get install -y \
    dsnutils \
    sudo \
    curl \
    openssh-client \
    git \
    locales-all 

# User creation
RUN addgroup --gid ${GROUP_ID} ${GROUP_NAME} \
    && adduser --shell /bin/bash --uid ${USER_ID} --gid ${GROUP_ID} ${USER_NAME} --disabled-password --comment "user for dev" 

# User sudo usage
RUN echo ${USER_NAME} ALL=\(root\) NOPASSWD:ALL > /etc/sudoers.d/${USER_NAME} \
    && chmod 0440 /etc/sudoers.d/${USER_NAME}

# User right on go directory
RUN chown ${USER_NAME}:${GROUP_NAME} /go -R

# Base directory config
ARG APP_PATH=/app
RUN mkdir -p ${APP_PATH} && chown ${USER_NAME}:${GROUP_NAME} ${APP_PATH}
WORKDIR $APP_PATH

EXPOSE 8000

USER $USER_NAME

