package lib

import (
	"fmt"
	"io"
	"strings"
)

type ShellColor int

var (
	Reset         ShellColor = 0
	Black         ShellColor = 30
	Red           ShellColor = 31
	Green         ShellColor = 32
	Yellow        ShellColor = 33
	Blue          ShellColor = 34
	Magenta       ShellColor = 35
	Cyan          ShellColor = 36
	Light_Gray    ShellColor = 37
	Gray          ShellColor = 90
	Light_Red     ShellColor = 91
	Light_Green   ShellColor = 92
	Light_Yellow  ShellColor = 93
	Light_Blue    ShellColor = 94
	Light_Magenta ShellColor = 95
	Light_Cyan    ShellColor = 96
	White         ShellColor = 97
)

func PrintLn(color ShellColor, content ...any) {
	contentString := fmt.Sprint(content...)
	fmt.Printf("\033[%vm%s\033[%vm\n", color, contentString, Reset)
}

func FprintLn(color ShellColor, writer io.Writer, content ...any) {
	contentString := fmt.Sprint(content...)
	fmt.Fprintf(writer, "\033[%vm%s\033[%vm\n", color, contentString, Reset)
}

func StringToColor(input string) (ShellColor, string) {

	input = strings.ToLower(input)
	input = strings.ReplaceAll(input, " ", "_")
	input = strings.ReplaceAll(input, "-", "_")

	switch input {
	case "reset":
		return Reset, "Reset"
	case "black":
		return Black, "Black"
	case "red":
		return Red, "Red"
	case "green":
		return Green, "Green"
	case "yellow":
		return Yellow, "Yellow"
	case "blue":
		return Blue, "Blue"
	case "magenta":
		return Magenta, "Magenta"
	case "cyan":
		return Cyan, "Cyan"
	case "light_gray":
		return Light_Gray, "Light_Gray"
	case "gray":
		return Gray, "Gray"
	case "light_red":
		return Light_Red, "Light_Red"
	case "light_green":
		return Light_Green, "Light_Green"
	case "light_yellow":
		return Light_Yellow, "Light_Yellow"
	case "light_blue":
		return Light_Blue, "Light_Blue"
	case "light_magenta":
		return Light_Magenta, "Light_Magenta"
	case "light_cyan":
		return Light_Cyan, "Light_Cyan"
	case "white":
		return White, "White"
	default:
		return Reset, "Reset"
	}
}
